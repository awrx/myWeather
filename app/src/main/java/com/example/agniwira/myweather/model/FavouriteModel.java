package com.example.agniwira.myweather.model;

import com.orm.SugarRecord;

/**
 * Created by agni.wira on 10/07/17.
 */

public class FavouriteModel extends SugarRecord {

    private String username;
    private String kota;

    public FavouriteModel(String username, String kota) {
        this.username = username;
        this.kota = kota;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }
}
