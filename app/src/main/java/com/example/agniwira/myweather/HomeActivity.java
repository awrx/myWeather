package com.example.agniwira.myweather;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.agniwira.myweather.adapter.ListMenuAdapter;
import com.example.agniwira.myweather.model.Weather;
import com.example.agniwira.myweather.task.WeatherTask;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //load dynamic
        final ListView listView = (ListView) findViewById(R.id.list_menu);
        ArrayList<Weather> listMenuItems = new ArrayList<>();


        //
        ListMenuAdapter listMenuAdapter = new ListMenuAdapter(HomeActivity.this, R.layout.list_menu_layout, listMenuItems);
        listView.setAdapter(listMenuAdapter);


        Button cekKota = (Button) findViewById(R.id.cekKota);

        cekKota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String namaKota = ((EditText) findViewById(R.id.kota)).getText().toString();
                new WeatherTask(HomeActivity.this).execute(namaKota);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("debugmenu", item.getItemId()+" "+ R.id.logout_menu);

        if(item.getItemId() == R.id.logout_menu){
            SharedPreferences sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear();
            editor.commit();

            final Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            getApplicationContext().startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }
}
