package com.example.agniwira.myweather.model;

/**
 * Created by agni.wira on 10/07/17.
 */

public class WeatherApi {

    private String temp_c;
    private String wind_mph;

    public WeatherApi(String temp_c, String wind_mph) {
        this.temp_c = temp_c;
        this.wind_mph = wind_mph;
    }

    public String getTemp_c() {
        return temp_c;
    }

    public void setTemp_c(String temp_c) {
        this.temp_c = temp_c;
    }

    public String getWind_mph() {
        return wind_mph;
    }

    public void setWind_mph(String wind_mph) {
        this.wind_mph = wind_mph;
    }
}
