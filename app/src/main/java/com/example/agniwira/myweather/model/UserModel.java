package com.example.agniwira.myweather.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by agni.wira on 10/07/17.
 */

public class UserModel extends SugarRecord {

    private String username;
    private String password;


    public UserModel(){}

    public UserModel(String username, String password) {

        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }




}
