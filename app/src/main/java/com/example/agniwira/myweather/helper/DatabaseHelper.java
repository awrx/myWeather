package com.example.agniwira.myweather.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.example.agniwira.myweather.model.UserModel;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import com.example.agniwira.myweather.schema.UserSchema.UserData;

/**
 * Created by agni.wira on 10/07/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "myapp.db";

    private static final String SQL_CREATE_TABLE = "CREATE TABLE "+ UserData.TABLE_NAME + "("+
            UserData._ID+ " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "+
            UserData.COLUMN_NAME_USERNAME+ " TEXT, "+
            UserData.COLUMN_NAME_PASSWORD+ " TEXT, "+
            UserData.COLUMN_NAME_FAVORIT+ " TEXT "+
            ")";
    private static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS "+ UserData.TABLE_NAME;

    private Context context;

    private SQLiteDatabase db;
    private String databasePath;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        databasePath = context.getExternalFilesDir(null) + "" + DATABASE_NAME;
        Log.d("tempat db", databasePath +"helper");
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("BUAT DB", !checkDb()+"buat db");
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DROP_TABLE);
        onCreate(sqLiteDatabase);

    }

    private boolean checkDb(){

        File db = new File(databasePath);
        if(db.exists()){
            Log.d("DEBUG db", "db exist");
            return true;
        }


        return false;

    }

    public void insertUser(UserModel user){
        db = this.getWritableDatabase();
        String sqlInsert = "INSERT INTO "+ UserData.TABLE_NAME +"("+
                UserData.COLUMN_NAME_USERNAME+","+
                UserData.COLUMN_NAME_PASSWORD+","+
                UserData.COLUMN_NAME_FAVORIT+
                ") VALUES (?, ?, ?)";

        if(db.isOpen()){
            Log.d("DEBUG db", "SQLITE OPEN");
            SQLiteStatement sqLiteStatement = db.compileStatement(sqlInsert);

            sqLiteStatement.bindString(1, user.getUsername());
            sqLiteStatement.bindString(2, user.getPassword());
            sqLiteStatement.bindString(3, user.getUsername());
            sqLiteStatement.execute();
            sqLiteStatement.clearBindings();
            //Log.d("DEBUG db", "finish insert record:" + new Gson().toJson(item));

        }

        db.close();
    }

    public int liatDb (){
        db = this.getReadableDatabase();
        int counter =0;

        Cursor c = db.rawQuery("SELECT * FROM user_data", null);
        if(c.moveToFirst()){
            do{
                //assing values
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                String column3 = c.getString(2);
                //Do something Here with values
                counter++;
                Log.d("isi database", column1+column2+column3);

            }while(c.moveToNext());
        }
        c.close();
        db.close();

        return counter;
    }

//    public ArrayList<ListMenuItem> getMenu(){
//        db = this.getReadableDatabase();
//        ArrayList<ListMenuItem> listMenuJSON = new ArrayList<>();
//
//        Cursor c = db.rawQuery("SELECT * FROM menu_item WHERE parent_id=0", null);
//        if(c.moveToFirst()){
//            do{
//                //assing values
//                long id = Long.parseLong(c.getString(0));
//                String iconUrl = c.getString(1);
//                String label = c.getString(2);
//                String description = c.getString(3);
//                int parentId = Integer.parseInt(c.getString(4));
//                Log.d("isi db", " "+parentId);
//                listMenuJSON.add(new ListMenuItem(id,iconUrl,label,description,parentId));
//            }while(c.moveToNext());
//        }
//        c.close();
//        db.close();
//
//        return listMenuJSON;
//    }

    public boolean isTableExists(String tableName, boolean openDb) {
        if(openDb) {
            if(db == null || !db.isOpen()) {
                db = getReadableDatabase();
            }

            if(!db.isReadOnly()) {
                db.close();
                db = getReadableDatabase();
            }
        }

        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public boolean isUserExist(String parent){
        Log.d("parentmenu", parent);

        db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM user_data WHERE username='"+parent+"'", null);
        boolean ada = false;

        if(c.moveToFirst()){
            do{
                return true;
            }while(c.moveToNext());
        }
        c.close();
        db.close();

        return false;
    }

    public boolean checkValid(String user, String password){

        db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM user_data WHERE username='"+user+"' and password='"+password+"'", null);
        boolean ada = false;

        if(c.moveToFirst()){
            do{
                return true;
            }while(c.moveToNext());
        }
        c.close();
        db.close();

        return false;


    }
}
