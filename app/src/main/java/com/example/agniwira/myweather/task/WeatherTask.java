package com.example.agniwira.myweather.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.agniwira.myweather.R;
import com.example.agniwira.myweather.helper.DatabaseHelper;
import com.example.agniwira.myweather.model.LocationAPi;
import com.example.agniwira.myweather.model.Weather;
import com.example.agniwira.myweather.model.WeatherApi;
import com.example.agniwira.myweather.service.WeatherService;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by agni.wira on 10/07/17.
 * http://api.openweathermap.org/data/2.5/weather?q=London&appid=0637f306fbc50db105ee809f5b3feda6
 */

public class WeatherTask extends AsyncTask<Object,Object,ArrayList<Weather>> {
    private Context context;
    private DatabaseHelper dbHelper;

    public WeatherTask(Context context) {
        this.context = context;

    }

    @Override
    protected void onPreExecute() {
        ProgressBar prg = (ProgressBar) (((Activity)context).findViewById(R.id.progressBar2));
        prg.setVisibility(View.VISIBLE);

        TextView cuaca = (TextView) (((Activity)context).findViewById(R.id.textCuaca));
        cuaca.setVisibility(View.INVISIBLE);

    }

    @Override
    protected ArrayList<Weather> doInBackground(Object... objects) {
        Retrofit client = new Retrofit.Builder()
                .baseUrl("http://api.apixu.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WeatherService service = client.create(WeatherService.class);

        String cekKota = (String) objects[0];

        Call<Weather> call = service.listWeather("096ed734ce06420284c62135171007",cekKota);

        Response<Weather> posts = null;

        try {
            posts = call.execute();
            Log.d("githubagni", new Gson().toJson(posts.body()));
            JSONObject resWeather  = new JSONObject(new Gson().toJson(posts.body()));
            ArrayList<Weather> item = new ArrayList<>();

            JSONObject arrWeather = resWeather.getJSONObject("current");
            String loc = (resWeather.getJSONObject("location")).getString("name");
            String temp = arrWeather.getString("temp_c");
            String wind = arrWeather.getString("wind_mph");
            Log.d("githubag", loc+temp+wind+"ayam");

            item.add(new Weather(new WeatherApi(temp,wind),new LocationAPi(loc)));
            return item;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<Weather> item = new ArrayList<>();


        return item;
    }

    @Override
    protected void onPostExecute(ArrayList<Weather> weathers) {
        ProgressBar prg = (ProgressBar) (((Activity)context).findViewById(R.id.progressBar2));
        prg.setVisibility(View.INVISIBLE);

        TextView cuaca = (TextView) (((Activity)context).findViewById(R.id.textCuaca));
        cuaca.setVisibility(View.VISIBLE);
        cuaca.setText((weathers.get(0).getLocation()).getName()+ ", "+(weathers.get(0).getCurrent()).getTemp_c()+" celcius, "+(weathers.get(0).getCurrent()).getTemp_c() +" wind mph");


    }

    //    private ArrayList<WeatherApi> createWeatherDesc(String menuString){
//        JSONArray jsonarray = null;
//        ArrayList<WeatherApi> listMenuJSON = new ArrayList<>();
//        try {
//            jsonarray = new JSONArray(menuString);
//            for (int i = 0; i < jsonarray.length(); i++) {
//                JSONObject jsonobject = jsonarray.getJSONObject(i);
//                String id = jsonobject.getString("id");
//                String iconUrl = jsonobject.getString("main");
//                String label = jsonobject.getString("description");
//                String description = jsonobject.getString("icon");
//               listMenuJSON.add(new WeatherApi(id,iconUrl,label,description));
//            }
//            return listMenuJSON;
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
}
