package com.example.agniwira.myweather.model;

import java.util.ArrayList;

/**
 * Created by agni.wira on 10/07/17.
 */

public class Weather {
    private long id;
    private WeatherApi current;
    private LocationAPi location;

    public Weather(WeatherApi current, LocationAPi location) {
        this.current = current;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WeatherApi getCurrent() {
        return current;
    }

    public void setCurrent(WeatherApi current) {
        this.current = current;
    }

    public LocationAPi getLocation() {
        return location;
    }

    public void setLocation(LocationAPi location) {
        this.location = location;
    }
    //    public String getMainWeather(){
//        return weather.get(0).getMain();
//    }
//
//    public String getDescWeather(){
//        return  weather.get(0).getDescription();
//    }
}
