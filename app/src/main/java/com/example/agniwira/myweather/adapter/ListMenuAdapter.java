package com.example.agniwira.myweather.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.agniwira.myweather.R;
import com.example.agniwira.myweather.model.Weather;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by agni.wira on 10/07/17.
 */

public class ListMenuAdapter extends ArrayAdapter<Weather> {
    private final LayoutInflater mInflater;
    private final ImageLoader imageLoader;
    private ArrayList<Weather> menuList;

    public ListMenuAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Weather> items) {
        super(context, resource);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageLoader.getInstance();
        menuList = items;

        if (!imageLoader.isInited()) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(options)
                    .build();
            imageLoader.init(config);
        }

    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Nullable
    @Override
    public Weather getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public int getPosition(@Nullable Weather item) {
        return super.getPosition(item);
    }

    @Override
    public long getItemId(int position) {
        return menuList.get(position).getId();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout layout;
        if(convertView == null){
            layout = (LinearLayout) mInflater.inflate(R.layout.list_menu_layout,parent,false);
        }else{
            layout = (LinearLayout) convertView;
        }
        //ImageView icon = (ImageView) layout.findViewById(R.id.menu_icon);
        TextView menuLabel = (TextView) layout.findViewById(R.id.menu_label);
        TextView menuDesc = (TextView) layout.findViewById(R.id.menu_desc);

        Weather itemMenu = menuList.get(position);
        //Log.e("1", (itemMenu.getIconUrl() == null) + "");

        //if(itemMenu.getIconUrl() != null && itemMenu.getIconUrl().length() >0)
         //   imageLoader.displayImage(itemMenu.getIconUrl(),icon);

        //menuLabel.setText(itemMenu.getMainWeather());
        //menuDesc.setText(itemMenu.getDescWeather());

        return layout;
    }
}
