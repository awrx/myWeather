package com.example.agniwira.myweather.schema;

import android.provider.BaseColumns;

/**
 * Created by agni.wira on 10/07/17.
 */

public class UserSchema {

    private UserSchema(){}

    public static class UserData implements BaseColumns {
        public static final String TABLE_NAME = "user_data";
        public static  String COLUMN_NAME_USERNAME =  "username";
        public static  String COLUMN_NAME_PASSWORD =  "password";
        public static  String COLUMN_NAME_FAVORIT =  "favorit_id";
    }
}
