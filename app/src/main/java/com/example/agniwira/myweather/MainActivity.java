package com.example.agniwira.myweather;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.agniwira.myweather.helper.DatabaseHelper;
import com.example.agniwira.myweather.model.UserModel;
import com.example.agniwira.myweather.task.WeatherTask;
import com.orm.SugarContext;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String NAMA = "nameKey";
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SugarContext.init(getApplicationContext());
        setContentView(R.layout.activity_main);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        final EditText username = (EditText) findViewById(R.id.edittext1);
        final EditText password = (EditText) findViewById(R.id.edittext2);
        Button helloButton = (Button) findViewById(R.id.buttonLogin);
        Button registerBtn = (Button) findViewById(R.id.buttonRegister);

        final DatabaseHelper dbHelper = new DatabaseHelper(MainActivity.this);

        if(sharedpreferences.getString("nameKey",null)!=null){
            Intent intent = new Intent(MainActivity.this,HomeActivity.class);
            (MainActivity.this).startActivity(intent);
        }

        helloButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               String strUsername = username.getText().toString();
               String strPass = password.getText().toString();
                Log.d("agniusername",strUsername);
               //List < UserModel > userModels = UserModel.findWithQuery(UserModel.class, "Select * from UserModel where username = ?", strUsername);
               List < UserModel > userModels = new ArrayList<>();

               //Toast.makeText(getApplicationContext(), userModels.toString(), Toast.LENGTH_SHORT).show();

               if(dbHelper.checkValid(strUsername,strPass)){
                    UserModel model = new UserModel(strUsername,strPass);
                   SharedPreferences.Editor editor = sharedpreferences.edit();

                   editor.putString(NAMA, strUsername);
                   editor.commit();

                    Toast.makeText(getApplicationContext(), dbHelper.liatDb()+"", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this,HomeActivity.class);

                    (MainActivity.this).startActivity(intent);

               } else{
                   Toast.makeText(getApplicationContext(), " Username atau password anda salah", Toast.LENGTH_SHORT).show();
               }
           }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Register.class);
                (MainActivity.this).startActivity(intent);
            }
        });



    }

    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }


}
